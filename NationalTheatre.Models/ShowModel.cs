﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.Models
{
    public class ShowModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Price { get; set; }

        public DateTime Date { get; set; }

        public int AvailableTickets { get; set; }

        public string DistributionList { get; set; }

        public ICollection<CategoryModel> Categories { get; set; }

        public List<int> SelectedCategories { get; set; }
    }
}
