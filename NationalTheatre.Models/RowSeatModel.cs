﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.Models
{
    public class RowSeatModel
    {
        public int Seat { get; set; }

        public string Row { get; set; }

        public int Id { get; set; }
    }
}
