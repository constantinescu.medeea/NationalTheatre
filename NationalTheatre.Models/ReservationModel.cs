﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.Models
{
    public class ReservationModel
    {
        public int Id { get; set; }

        public string Show { get; set; }

        public int ShowId { get; set; }

        public DateTime Date { get; set; }

        public RowSeatModel RowSeat { get; set; }
    }
}
