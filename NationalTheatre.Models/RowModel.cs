﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.Models
{
    public class RowModel
    {
        public int Id { get; set; }

        public string Nr { get; set; }
    }
}
