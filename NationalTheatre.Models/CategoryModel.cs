﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.Models
{
    public class CategoryModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
