﻿USE [master]
GO
/****** Object:  Database [Theatre]    Script Date: 3/15/2018 1:41:18 PM ******/
CREATE DATABASE [Theatre]
GO
ALTER DATABASE [Theatre] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Theatre].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Theatre] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Theatre] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Theatre] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Theatre] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Theatre] SET ARITHABORT OFF 
GO
ALTER DATABASE [Theatre] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Theatre] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Theatre] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Theatre] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Theatre] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Theatre] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Theatre] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Theatre] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Theatre] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Theatre] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Theatre] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Theatre] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Theatre] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Theatre] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Theatre] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Theatre] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Theatre] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Theatre] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Theatre] SET  MULTI_USER 
GO
ALTER DATABASE [Theatre] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Theatre] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Theatre] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Theatre] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Theatre] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Theatre] SET QUERY_STORE = OFF
GO
USE [Theatre]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [Theatre]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 3/15/2018 1:41:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cashiers]    Script Date: 3/15/2018 1:41:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cashiers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[Password] [nvarchar](max) NULL,
	[Username] [nvarchar](max) NULL,
 CONSTRAINT [PK_Cashiers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 3/15/2018 1:41:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ShowCashiers]    Script Date: 3/15/2018 1:41:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShowCashiers](
	[ShowId] [int] NOT NULL,
	[CashierId] [int] NOT NULL,
 CONSTRAINT [PK_ShowCashiers] PRIMARY KEY CLUSTERED 
(
	[ShowId] ASC,
	[CashierId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ShowCategories]    Script Date: 3/15/2018 1:41:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShowCategories](
	[ShowId] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
 CONSTRAINT [PK_ShowCategories] PRIMARY KEY CLUSTERED 
(
	[ShowId] ASC,
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Shows]    Script Date: 3/15/2018 1:41:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shows](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AvailableTickets] [int] NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[DistributionList] [nvarchar](max) NULL,
	[Name] [nvarchar](max) NULL,
	[Price] [int] NOT NULL,
 CONSTRAINT [PK_Shows] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Index [IX_ShowCashiers_CashierId]    Script Date: 3/15/2018 1:41:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_ShowCashiers_CashierId] ON [dbo].[ShowCashiers]
(
	[CashierId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ShowCategories_CategoryId]    Script Date: 3/15/2018 1:41:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_ShowCategories_CategoryId] ON [dbo].[ShowCategories]
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ShowCashiers]  WITH CHECK ADD  CONSTRAINT [FK_ShowCashiers_Cashiers_CashierId] FOREIGN KEY([CashierId])
REFERENCES [dbo].[Cashiers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ShowCashiers] CHECK CONSTRAINT [FK_ShowCashiers_Cashiers_CashierId]
GO
ALTER TABLE [dbo].[ShowCashiers]  WITH CHECK ADD  CONSTRAINT [FK_ShowCashiers_Shows_ShowId] FOREIGN KEY([ShowId])
REFERENCES [dbo].[Shows] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ShowCashiers] CHECK CONSTRAINT [FK_ShowCashiers_Shows_ShowId]
GO
ALTER TABLE [dbo].[ShowCategories]  WITH CHECK ADD  CONSTRAINT [FK_ShowCategories_Categories_CategoryId] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Categories] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ShowCategories] CHECK CONSTRAINT [FK_ShowCategories_Categories_CategoryId]
GO
ALTER TABLE [dbo].[ShowCategories]  WITH CHECK ADD  CONSTRAINT [FK_ShowCategories_Shows_ShowId] FOREIGN KEY([ShowId])
REFERENCES [dbo].[Shows] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ShowCategories] CHECK CONSTRAINT [FK_ShowCategories_Shows_ShowId]
GO
USE [master]
GO
ALTER DATABASE [Theatre] SET  READ_WRITE 
GO
