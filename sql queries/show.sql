﻿CREATE TABLE [dbo].Show
(
    [Id] INT NOT NULL PRIMARY KEY IDENTITY,
    [Name] NVARCHAR(256) NOT NULL,
    [Price] MONEY NULL,
    [Seats] INT NOT NULL
)
 