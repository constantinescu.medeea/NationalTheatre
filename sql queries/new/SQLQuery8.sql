CREATE TABLE [dbo].[ShowCategories](
	[ShowId] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
 CONSTRAINT [PK_ShowCategories] PRIMARY KEY CLUSTERED 
(
	[ShowId] ASC,
	[CategoryId] ASC
)) ON [PRIMARY]
GO
