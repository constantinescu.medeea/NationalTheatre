CREATE TABLE [dbo].[Shows](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AvailableTickets] [int] NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[DistributionList] [nvarchar](max) NULL,
	[Name] [nvarchar](max) NULL,
	[Price] [int] NOT NULL,
 CONSTRAINT [PK_Shows] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
))
GO
