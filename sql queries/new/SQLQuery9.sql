CREATE TABLE [dbo].[ShowCashiers](
	[ShowId] [int] NOT NULL,
	[CashierId] [int] NOT NULL,
 CONSTRAINT [PK_ShowCashiers] PRIMARY KEY CLUSTERED 
(
	[ShowId] ASC,
	[CashierId] ASC
)) ON [PRIMARY]
GO
