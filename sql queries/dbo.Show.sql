﻿CREATE TABLE [dbo].[Show] (
    [Id]    INT            IDENTITY (1, 1) NOT NULL,
    [Name]  NVARCHAR (256) NOT NULL,
    [Price] INT          NULL,
    [Seats] INT            NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

