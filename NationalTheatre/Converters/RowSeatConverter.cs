﻿using NationalTheatre.Models;
using NationalTheatre.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NationalTheatre.Converters
{
    public static class RowSeatConverter
    {
        public static RowSeatViewModel ToRowSeatViewModel(this RowSeatModel rowSeatModel)
        {
            if (rowSeatModel == null)
            {
                return null;
            }

            RowSeatViewModel rowSeatViewModel = new RowSeatViewModel
            {
                Row = rowSeatModel.Row,
                Seat = rowSeatModel.Seat,
                Id = rowSeatModel.Id
            };

            return rowSeatViewModel;
        }

        public static RowSeatModel ToRowSeatModel(this RowSeatViewModel rowSeatViewModel)
        {
            if (rowSeatViewModel == null)
            {
                return null;
            }

            RowSeatModel rowSeatModel = new RowSeatModel
            {
                Row = rowSeatViewModel.Row,
                Seat = rowSeatViewModel.Seat,
                Id = rowSeatViewModel.Id
            };

            return rowSeatModel;
        }
    }
}
