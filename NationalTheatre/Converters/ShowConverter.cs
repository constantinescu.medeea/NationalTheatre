﻿using NationalTheatre.Models;
using NationalTheatre.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NationalTheatre.Converters
{
    public static class ShowConverter
    {
        public static ShowViewModel ToShowViewModel(this ShowModel showModel)
        {
            if (showModel == null)
            {
                return null;
            }

            ShowViewModel showViewModel = new ShowViewModel
            {
                Id = showModel.Id,
                Name = showModel.Name,
                Price = showModel.Price,
                AvailableTickets = showModel.AvailableTickets,
                Date = showModel.Date,
                DistributionList = showModel.DistributionList,
                SelectedCategories = showModel.SelectedCategories,
                Categories = new List<CategoryViewModel>()
            };

            if (showModel.Categories != null)
            {
                foreach (var category in showModel.Categories)
                {
                    showViewModel.Categories.Add(category.ToCategoryViewModel());
                }
            }

            return showViewModel;
        }

        public static ShowModel ToShowModel(this ShowViewModel showViewModel)
        {
            if (showViewModel == null)
            {
                return null;
            }

            ShowModel showModel = new ShowModel
            {
                Id = showViewModel.Id,
                Name = showViewModel.Name,
                Price = showViewModel.Price,
                AvailableTickets = showViewModel.AvailableTickets,
                Date = showViewModel.Date,
                DistributionList = showViewModel.DistributionList,
                SelectedCategories = showViewModel.SelectedCategories,
                Categories = new List<CategoryModel>()
            };

            if (showViewModel.Categories != null)
            {
                foreach (var category in showViewModel.Categories)
                {
                    showModel.Categories.Add(category.ToCategoryModel());
                }
            }

            return showModel;
        }
    }
}
