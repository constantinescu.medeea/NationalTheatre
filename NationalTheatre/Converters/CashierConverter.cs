﻿using NationalTheatre.Models;
using NationalTheatre.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NationalTheatre.Converters
{
    public static class CashierConverter
    {
        public static CashierViewModel ToCashierViewModel(this CashierModel cashierModel)
        {
            if (cashierModel == null)
            {
                return null;
            }

            CashierViewModel cashierViewModel = new CashierViewModel
            {
                Id = cashierModel.Id,
                FirstName = cashierModel.FirstName,
                LastName = cashierModel.LastName,
                Username = cashierModel.Username,
                Password = cashierModel.Password,
                Role = cashierModel.Role
            };

            return cashierViewModel;
        }

        public static CashierModel ToCashierModel(this CashierViewModel cashierViewModel)
        {
            if (cashierViewModel == null)
            {
                return null;
            }

            CashierModel cashierModel = new CashierModel
            {
                Id = cashierViewModel.Id,
                FirstName = cashierViewModel.FirstName,
                LastName = cashierViewModel.LastName,
                Username = cashierViewModel.Username,
                Password = cashierViewModel.Password,
                Role = cashierViewModel.Role
            };

            return cashierModel;
        }

    }
}
