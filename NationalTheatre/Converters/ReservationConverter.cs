﻿using NationalTheatre.Models;
using NationalTheatre.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NationalTheatre.Converters
{
    public static class ReservationConverter
    {
        public static ReservationViewModel ToReservationViewModel(this ReservationModel reservationModel)
        {
            if (reservationModel == null)
            {
                return null;
            }

            ReservationViewModel reservationViewModel = new ReservationViewModel
            {
                Id = reservationModel.Id,
                Date = reservationModel.Date,
                Show = reservationModel.Show,
                RowSeat = reservationModel.RowSeat.ToRowSeatViewModel(),
                ShowId = reservationModel.ShowId
            };

            return reservationViewModel;
        }

        public static ReservationModel ToReservationModel(this ReservationViewModel reservationViewModel)
        {
            if (reservationViewModel == null)
            {
                return null;
            }

            ReservationModel reservationModel = new ReservationModel
            {
                Id = reservationViewModel.Id,
                Date = reservationViewModel.Date,
                Show = reservationViewModel.Show,
                RowSeat = reservationViewModel.RowSeat.ToRowSeatModel(),
                ShowId = reservationViewModel.ShowId
            };

            return reservationModel;
        }
    }
}
