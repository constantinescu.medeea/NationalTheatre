﻿using NationalTheatre.Models;
using NationalTheatre.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NationalTheatre.Converters
{
    public static class CategoryConverter
    {
        public static CategoryViewModel ToCategoryViewModel(this CategoryModel categoryModel)
        {
            if (categoryModel == null)
            {
                return null;
            }

            CategoryViewModel categoryViewModel = new CategoryViewModel
            {
                Id = categoryModel.Id,
                Name = categoryModel.Name                
            };

            return categoryViewModel;
        }

        public static CategoryModel ToCategoryModel(this CategoryViewModel categoryViewModel)
        {
            if (categoryViewModel == null)
            {
                return null;
            }

            CategoryModel categoryModel = new CategoryModel
            {
                Id = categoryViewModel.Id,
                Name = categoryViewModel.Name
            };

            return categoryModel;
        }
    }
}
