﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NationalTheatre.ViewModels
{
    public class ShowViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Required")]
        public int Price { get; set; }

        [Required(ErrorMessage = "Required")]
        [Range(0, 15, ErrorMessage = "The capacity of the National Theatre is 15!")]
        public int AvailableTickets { get; set; }

        [Required(ErrorMessage = "Required")]
        public DateTime Date { get; set; }

        [Required(ErrorMessage = "Required")]
        public string DistributionList { get; set; }

        [Required(ErrorMessage = "Required")]
        public List<CategoryViewModel> Categories { get; set; }

        [Required(ErrorMessage = "Required")]
        public List<CategoryViewModel> AllCategories { get; set; }

        public List<int> SelectedCategories { get; set; }

        public List<int> AllSeats { get; set; }

        public List<string> AllRows { get; set; }

        public List<RowSeatViewModel> RowSeats { get; set; }

        public List<RowSeatViewModel> ChosenRowSeats { get; set; }
    }
}
