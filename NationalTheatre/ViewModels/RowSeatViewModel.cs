﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NationalTheatre.ViewModels
{
    public class RowSeatViewModel
    {
        public int Id { get; set; }

        public int Seat { get; set; }

        public string Row { get; set; }

        public bool Checkbox { get; set; }

        public string RowSeat()
        {
            return Row + Seat.ToString();
        }

        public bool RadioButtonAnswer { get; set; }
    }
}
