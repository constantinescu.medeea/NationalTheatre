﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NationalTheatre.ViewModels
{
    public class ReservationViewModel
    {
        public int Id { get; set; }

        public string Show { get; set; }

        public DateTime Date { get; set; }

        public RowSeatViewModel RowSeat { get; set; }

        public int ShowId { get; set; }

        public List<RowSeatViewModel> RowSeats { get; set; }

        public int NewRowSeat { get; set; }

        public RowSeatViewModel ChosenRowSeat { get; set; }
    }
}
