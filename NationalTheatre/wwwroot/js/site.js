﻿$("input[id=chk]").change(function () {
  var max = $('#tickets').val();
  if ($("input[id=chk]:checked").length.valueOf() >= max) {
    $("input[id=chk]").attr('disabled', 'disabled');
    $("input[id=chk]:checked").removeAttr('disabled');
  } else {
    $("input[id=chk]").removeAttr('disabled');
  }
});