﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NationalTheatre.BusinessLogic.Extensions;
using NationalTheatre.BusinessLogic.ILogic;
using NationalTheatre.Converters;
using NationalTheatre.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NationalTheatre.Controllers
{
    [Authorize(Policy = "AdminsOnly")]
    public class CashiersController : Controller
    {
        private ICashierLogic _iCashierLogic;
       
        public CashiersController(ICashierLogic iCashierLogic)
        {
            _iCashierLogic = iCashierLogic;
        }

        public IActionResult Index()
        {
            var cashier = _iCashierLogic.GetCashiers();
            var cashierViewModel = cashier.Select(c => c.ToCashierViewModel());

            return View(cashierViewModel);
        }

        [HttpGet]
        public IActionResult Create()
        {
            CashierViewModel cashierViewModel = new CashierViewModel();

            return View(cashierViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CashierViewModel cashierViewModel)
        {
            if (!_iCashierLogic.VerifyUsername(cashierViewModel.Username))
            {
                cashierViewModel.Password = cashierViewModel.Password.Encrypt();
                cashierViewModel.Role = "cashier";
                var cashierModel = cashierViewModel.ToCashierModel();

                _iCashierLogic.CreateCashier(cashierModel);
                return RedirectToAction("Index", "Cashiers");
            }

            ModelState.AddModelError("", "This username is already in use.");
            return View();
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            var cashier = _iCashierLogic.GetById(id);
            var cashierViewModel = cashier.ToCashierViewModel();

            return View(cashierViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(CashierViewModel cashierViewModel)
        {
            var cashierModel = _iCashierLogic.GetById(cashierViewModel.Id);      
           
            _iCashierLogic.DeleteCashier(cashierModel.Id);

            return RedirectToAction("Index", "Cashiers");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var cashierModel = _iCashierLogic.GetById(id);
            var cashierViewModel = cashierModel.ToCashierViewModel();
            cashierViewModel.Password = null;
            cashierViewModel.Role = "cashier";
            cashierViewModel.CurrentUsername = cashierViewModel.Username;
            return View(cashierViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(CashierViewModel cashierViewModel)
        {
            if (!cashierViewModel.CurrentUsername.Equals(cashierViewModel.Username))
            {
                if (!_iCashierLogic.VerifyUsername(cashierViewModel.Username))
                {
                    cashierViewModel.Password = cashierViewModel.Password.Encrypt();

                    var cashierModel = cashierViewModel.ToCashierModel();
                    _iCashierLogic.UpdateCashier(cashierModel);
                    return RedirectToAction("Index", "Cashiers");
                }
            }
            else
            {
                cashierViewModel.Password = cashierViewModel.Password.Encrypt();

                var cashierModel = cashierViewModel.ToCashierModel();
                _iCashierLogic.UpdateCashier(cashierModel);
                return RedirectToAction("Index", "Cashiers");
            }

            ModelState.AddModelError("", "This username is already in use.");
            return View();
        }   
    }
}
