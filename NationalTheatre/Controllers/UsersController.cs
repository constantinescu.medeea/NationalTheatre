﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using NationalTheatre.BusinessLogic.ILogic;
using NationalTheatre.IExtensions;
using NationalTheatre.ViewModels;
using NationalTheatre.BusinessLogic.Extensions;
using NationalTheatre.Converters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

namespace NationalTheatre.Controllers
{
    
    public class UsersController : Controller
    {
        public ICashierLogic _iCashierLogic;
        public ILoginHandler _iLoginHandler;
        private IShowLogic _iShowLogic;
        private IHttpContextAccessor _httpContextAccessor;
        private IShowCashierLogic _iShowCashierLogic;

        public UsersController(ICashierLogic iCashierLogic, ILoginHandler iLoginHandler, 
            IShowLogic iShowLogic, IHttpContextAccessor iHttpContextAccessor,
            IShowCashierLogic iShowCashierLogic)
        {
            _iCashierLogic = iCashierLogic;
            _iShowLogic = iShowLogic;
            _iLoginHandler = iLoginHandler;
            _httpContextAccessor = iHttpContextAccessor;
            _iShowCashierLogic = iShowCashierLogic;
        }

        public IActionResult Index()
        {
            var show = _iShowLogic.GetCurrentShows();
            var showViewModel = show.Select(s => s.ToShowViewModel()).ToList();
            TempData.Keep();
            return View(showViewModel);
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(CashierViewModel cashierViewModel)
        {
            var nr = _iCashierLogic.CountRecords(cashierViewModel.Username);
            if (nr != 0)
            {
                var loginVerification = _iCashierLogic.VerifyUser(cashierViewModel.Username, cashierViewModel.Password.Encrypt());

                if (loginVerification)
                {
                    var userModel = _iCashierLogic.GetUserByUsername(cashierViewModel.Username);
                    var userViewModel = userModel.ToCashierViewModel();
                    return await _iLoginHandler.RedirectUser(userViewModel);
                }
            }
            
            ModelState.AddModelError("", "Invalid username or password.");

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            return await _iLoginHandler.SignOut();

        }

        [HttpGet]
        public IActionResult Reserve(int id)
        {
            var show = _iShowLogic.GetById(id);
            var showViewModel = show.ToShowViewModel();
            var showModel = _iShowLogic.GetById(showViewModel.Id);
            var reservationVerification = _iShowLogic.VerifyReservation(id);
           
            var allRowSeats = new List<RowSeatViewModel>();
            allRowSeats = _iShowLogic.GetAvailableRowSeatList(id)
                .Select(r => r.ToRowSeatViewModel()).ToList();
            showViewModel.RowSeats = new List<RowSeatViewModel>();
            showViewModel.RowSeats = allRowSeats;

            if (reservationVerification)
            {
                
                return View(showViewModel);
            }

            TempData["error"] = "No available tickets!";
            return RedirectToAction("Index", "Users");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Reserve(ShowViewModel showViewModel)
        {
            var rowSeats = showViewModel.RowSeats;
            var chosenRowSeats = new List<RowSeatViewModel>();

            foreach (var rowSeat in rowSeats)
            {
                if (rowSeat.Checkbox)
                {
                    chosenRowSeats.Add(rowSeat);
                }
            }
            var rowSeatsModel = chosenRowSeats.Select(c => c.ToRowSeatModel()).ToList();

            var user = HttpContext.User.Identity.Name;

            _iShowCashierLogic.ReserveRowSeat(user, showViewModel.Id, rowSeatsModel, DateTime.Now);

            return RedirectToAction("Index", "Users");           
        }

        [HttpGet]
        public IActionResult ViewReservations(int id)
        {
            var reservationList = _iShowCashierLogic.ViewReservations(id)
                .Select(r => r.ToReservationViewModel()).ToList();

            return View(reservationList);
        }

        [HttpGet]
        public IActionResult EditReservation(int id)
        {
            var reservation = _iShowCashierLogic.GetReservation(id).ToReservationViewModel();
            var availableRowSeat = _iShowLogic.GetAvailableRowSeatList(reservation.ShowId)
                .Select(r => r.ToRowSeatViewModel()).ToList();
            reservation.RowSeats = new List<RowSeatViewModel>();
            reservation.RowSeats = availableRowSeat;

            return View(reservation);
        }

        [HttpPost]
        public IActionResult EditReservation(ReservationViewModel reservationViewModel)
        {
            
            foreach (var rowSeat in reservationViewModel.RowSeats)
            {
                if (rowSeat.Id == reservationViewModel.NewRowSeat)
                {
                    reservationViewModel.ChosenRowSeat = rowSeat;
                }
            }

            _iShowCashierLogic.UpdateRowSeat(reservationViewModel.Id,
                reservationViewModel.RowSeat.ToRowSeatModel(), reservationViewModel.ChosenRowSeat.ToRowSeatModel());
            return RedirectToAction("Index", "Users");
        }

        [HttpGet]
        public IActionResult DeleteReservation(int id)
        {
            var reservation = _iShowCashierLogic.GetReservation(id).ToReservationViewModel();

            return View(reservation);
        }

        [HttpPost]
        public IActionResult DeleteReservation(ReservationViewModel reservationViewModel)
        {
            _iShowCashierLogic.DeleteReservation(reservationViewModel.Id, reservationViewModel.ShowId);
            
            return RedirectToAction("ViewReservations", new RouteValueDictionary(
                                    new { controller = "Users", action = "ViewReservations",
                                        id = reservationViewModel.ShowId }));
        }
    }
}
