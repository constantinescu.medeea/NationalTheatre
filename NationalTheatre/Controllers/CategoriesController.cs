﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NationalTheatre.BusinessLogic.ILogic;
using NationalTheatre.Converters;
using NationalTheatre.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NationalTheatre.Controllers
{
    [Authorize(Policy = "AdminsOnly")]
    public class CategoriesController : Controller
    {
        private ICategoryLogic _iCategoryLogic;

        public CategoriesController(ICategoryLogic iCategoryLogic)
        {
            _iCategoryLogic = iCategoryLogic;
        }

        public IActionResult Index()
        {
            var category = _iCategoryLogic.GetCategories();
            var categoryViewModel = category.Select(c => c.ToCategoryViewModel());

            return View(categoryViewModel);
        }

        [HttpGet]
        public IActionResult Create()
        {
            CategoryViewModel categoryViewModel = new CategoryViewModel();

            return View(categoryViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CategoryViewModel categoryViewModel)
        {
            var categoryModel = categoryViewModel.ToCategoryModel();
            _iCategoryLogic.CreateCategory(categoryModel);

            return RedirectToAction("Index", "Categories");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            var category = _iCategoryLogic.GetById(id);
            var categoryViewModel = category.ToCategoryViewModel();

            return View(categoryViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(CategoryViewModel categoryViewModel)
        {
            var categoryModel = categoryViewModel.ToCategoryModel();
            _iCategoryLogic.DeleteCategory(categoryModel.Id);

            return RedirectToAction("Index", "Categories");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var categoryModel = _iCategoryLogic.GetById(id);
            var categoryViewModel = categoryModel.ToCategoryViewModel();

            return View(categoryViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(CategoryViewModel categoryViewModel)
        {

            var categoryModel = categoryViewModel.ToCategoryModel();
            _iCategoryLogic.UpdateCategory(categoryModel);

            return RedirectToAction("Index", "Categories");
        }
    }
}
