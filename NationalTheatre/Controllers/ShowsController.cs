﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NationalTheatre.BusinessLogic.Export;
using NationalTheatre.BusinessLogic.ILogic;
using NationalTheatre.Converters;
using NationalTheatre.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NationalTheatre.Controllers
{
    [Authorize(Policy = "AdminsOnly")]
    public class ShowsController : Controller
    {
        private IShowLogic _iShowLogic;
        private ICategoryLogic _iCategoryLogic;
        private IShowCashierLogic _iShowCashierLogic;

        public ShowsController(IShowLogic iShowLogic, ICategoryLogic iCategoryLogic,
            IShowCashierLogic iShowCashierLogic)
        {
            _iShowLogic = iShowLogic;
            _iCategoryLogic = iCategoryLogic;
            _iShowCashierLogic = iShowCashierLogic;
        }

        public IActionResult Index()
        {
            var show = _iShowLogic.GetShows();
            var showViewModel = show.Select(s => s.ToShowViewModel());

            return View(showViewModel);
        }

        [HttpGet]
        public IActionResult Create()
        {
            ShowViewModel showViewModel = new ShowViewModel();
            showViewModel.Date = DateTime.Now;
            var categoryList = _iCategoryLogic.GetCategories();
            var categoryViewModelList = categoryList.Select(c => c.ToCategoryViewModel()).ToList();
            showViewModel.AllCategories = categoryViewModelList;

            return View(showViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ShowViewModel showViewModel)
        {
            showViewModel.SelectedCategories = new List<int>();

            foreach (var category in showViewModel.AllCategories)
            {
                if (category.CheckboxAnswer)
                {
                    showViewModel.SelectedCategories.Add(category.Id);
                }
            }

            var showModel = showViewModel.ToShowModel();

            _iShowLogic.CreateShow(showModel);
            var showId = _iShowLogic.GetIdOfShow(showModel.Name);
            _iShowLogic.AddCategoriesToShow(showViewModel.SelectedCategories, showId);

            return RedirectToAction("Index", "Shows");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            var show = _iShowLogic.GetById(id);
            var showViewModel = show.ToShowViewModel();

            return View(showViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(ShowViewModel showViewModel)
        {
            _iShowLogic.DeleteShow(showViewModel.Id);

            return RedirectToAction("Index", "Shows");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var showModel = _iShowLogic.GetById(id);
            var showViewModel = showModel.ToShowViewModel();
            var categoryList = _iShowLogic.GetCategoriesOfShow(id);
            var categories = new List<CategoryViewModel>();

            var categoryFullList = _iCategoryLogic.GetCategories();

            foreach (var categoryId in categoryList)
            {
                categories.Add(_iCategoryLogic.GetById(categoryId).ToCategoryViewModel());
            }
            showViewModel.AllCategories = categoryFullList.Select(c => c.ToCategoryViewModel()).ToList();

            foreach (var category in categories)
            {
                foreach (var c in showViewModel.AllCategories)
                {
                    if (category.Id == c.Id)
                    {
                        c.CheckboxAnswer = true;
                    }
                }
            }

            return View(showViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(ShowViewModel showViewModel)
        {
            showViewModel.SelectedCategories = new List<int>();

            foreach (var category in showViewModel.AllCategories)
            {
                if (category.CheckboxAnswer)
                {
                    showViewModel.SelectedCategories.Add(category.Id);
                }
            }

            var showModel = showViewModel.ToShowModel();
            _iShowLogic.UpdateShow(showModel);
            _iShowLogic.DeleteShowCategories(showModel);
            _iShowLogic.AddCategoriesToShow(showViewModel.SelectedCategories, showModel.Id);

            return RedirectToAction("Index", "Shows");
        }

        [HttpGet]
        public IActionResult ExportToCsv(int id)
        {
            var show = _iShowLogic.GetById(id);
            var reservations = _iShowCashierLogic.ViewReservations(id);
            var showName = _iShowLogic.GetById(id).Name;

            ExportFactory factory = new ExportFileFactory();
            IExportFile csvFile = factory.GetFile("csv");
            csvFile.Export(reservations, showName);

            return RedirectToAction("Index", "Shows");
        }

        [HttpGet]
        public IActionResult ExportToXml(int id)
        {
            var show = _iShowLogic.GetById(id);
            var reservations = _iShowCashierLogic.ViewReservations(id);
            var showName = _iShowLogic.GetById(id).Name;

            ExportFactory factory = new ExportFileFactory();
            IExportFile csvFile = factory.GetFile("xml");
            csvFile.Export(reservations, showName);

            return RedirectToAction("Index", "Shows");
        }
    }
}
