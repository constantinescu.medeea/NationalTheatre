﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NationalTheatre.ViewModels;

namespace NationalTheatre.IExtensions
{
    public interface ILoginHandler
    {
        Task<IActionResult> RedirectUser(CashierViewModel cashierViewModel);

        Task<IActionResult> SignOut();
    }
}
