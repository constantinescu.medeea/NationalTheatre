﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NationalTheatre.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NationalTheatre.ViewComponents
{
    public class LoginViewComponent : ViewComponent
    {
        public IHttpContextAccessor _iHttpContextAccessor;

        public LoginViewComponent(IHttpContextAccessor iHttpContextAccessor)
        {
            _iHttpContextAccessor = iHttpContextAccessor;
        }

        public IViewComponentResult Invoke()
        {
            if (_iHttpContextAccessor.HttpContext.User.Identity.IsAuthenticated)
            {
                var loggedInUser = HttpContext.User;
                var loggedInUsername = loggedInUser.Identity.Name;

                LoginViewModel model = new LoginViewModel
                {
                    Username = loggedInUsername
                };

            return View(model);
            }
            return View("Login");
        }
    }
}
