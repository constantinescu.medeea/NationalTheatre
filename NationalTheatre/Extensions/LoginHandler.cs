﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NationalTheatre.IExtensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using NationalTheatre.ViewModels;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace NationalTheatre.Extensions
{
    public class LoginHandler : ILoginHandler
    {
        private IHttpContextAccessor _httpContextAccessor;

        public LoginHandler(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<IActionResult> RedirectUser(CashierViewModel cashierViewModel)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, cashierViewModel.Username),
                new Claim(cashierViewModel.Role, cashierViewModel.Role)
            };

            var useridentity = new ClaimsIdentity(claims, "login");

            ClaimsPrincipal principal = new ClaimsPrincipal(useridentity);

            await _httpContextAccessor.HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

            if (cashierViewModel.Role.Equals("cashier"))
            {
                return new RedirectToActionResult("Index", "Users", null);
            }

            return new RedirectToActionResult("Index", "Shows", null);
        }

        public async Task<IActionResult> SignOut()
        {
            
            await _httpContextAccessor.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            
            return new RedirectToActionResult("Login", "Users", null);
        }
    }
}
