﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.Dal.Models
{
    public class Reservation
    {
        public int Id { get; set; }

        public string Show { get; set; }

        public int ShowId { get; set; }

        public DateTime Date { get; set; }

        public RowSeat RowSeat { get; set; }
    }
}
