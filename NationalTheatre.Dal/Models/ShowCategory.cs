﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.Dal.Models
{
    public class ShowCategory
    {
        public int ShowId { get; set; }

        public Show Show { get; set; }

        public int CategoryId { get; set; }

        public Category Category { get; set; }
    }
}
