﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.Dal.Models
{
    public class Seat
    {
        public int Id { get; set; }

        public int Nr { get; set; }
    }
}
