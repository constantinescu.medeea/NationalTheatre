﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.Dal.Models
{
    public class Row
    {
        public int Id { get; set; }

        public string Nr { get; set; }
    }
}
