﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.Dal.Models
{
    public class RowSeat
    {
        public int Seat { get; set; }

        public string Row { get; set; }
    }
}
