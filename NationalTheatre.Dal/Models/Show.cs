﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.Dal.Models
{
    public class Show
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Price { get; set; }

        public int AvailableTickets { get; set; }

        public DateTime Date { get; set; }

        public string DistributionList { get; set; }

        public ICollection<ShowCategory> ShowCategories { get; set; }
    }
}
