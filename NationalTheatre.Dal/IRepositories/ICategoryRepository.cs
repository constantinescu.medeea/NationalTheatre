﻿using NationalTheatre.Dal.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace NationalTheatre.Dal.IRepositories
{
    public interface ICategoryRepository : IGenericRepository<Category>
    {
    }
}
