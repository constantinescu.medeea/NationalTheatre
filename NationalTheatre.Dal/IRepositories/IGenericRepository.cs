﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace NationalTheatre.Dal.IRepositories
{
    public interface IGenericRepository<T>
    {
        List<T> GetRecords();

        void DeleteRecord(int id);

        T GetRecord(int id);

        void InsertRecord(T entity);

        void UpdateRecord(T entity);
    }
}
