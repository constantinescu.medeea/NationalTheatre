﻿using NationalTheatre.Dal.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.Dal.IRepositories
{
    public interface IShowCashierRepository
    {
        void ReserveRowSeat(int cashierId, int showId, RowSeat rowSeat, DateTime date);

        List<Reservation> GetReservationsOfShow(int showId);

        Reservation GetReservation(int id);

        void UpdateRowSeat(int reservationId, RowSeat rowSeat, RowSeat chosenRowSeat);

        void DeleteReservation(int id);
    }
}
