﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.Dal.IRepositories
{
    public interface ISeatRepository
    {
        List<int> GetAllSeats();

    }
}
