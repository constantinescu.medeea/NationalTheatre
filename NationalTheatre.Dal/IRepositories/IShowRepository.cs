﻿using NationalTheatre.Dal.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace NationalTheatre.Dal.IRepositories
{
    public interface IShowRepository : IGenericRepository<Show>
    {
        int GetIdOfShow(string name);

        List<int> GetCategoryIdOfShow(int id);

        void InsertCategoryToShow(int showId, int categoryId);

        void UpdateShowCategoryRecord(int showId, int categoryId);

        void DeleteShowCategoryRecord(int showId);

        List<RowSeat> GetOccupiedSeatRowOfShow(int showId);
    }
}
