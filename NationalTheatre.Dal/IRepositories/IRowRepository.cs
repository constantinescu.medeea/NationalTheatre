﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.Dal.IRepositories
{
    public interface IRowRepository
    {
        List<string> GetAllRows();
    }
}
