﻿using NationalTheatre.Dal.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.Dal.IRepositories
{
    public interface ICashierRepository : IGenericRepository<Cashier>
    {
        Cashier GetCashierByUsername(string username);

        int CountRecords(string username);
    }
}
