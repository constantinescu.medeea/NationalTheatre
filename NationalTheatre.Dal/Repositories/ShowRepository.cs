﻿using NationalTheatre.Dal.IRepositories;
using NationalTheatre.Dal.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Data;

namespace NationalTheatre.Dal.Repositories
{
    public class ShowRepository : IShowRepository
    {
        SqlConnection conn;

        public ShowRepository()
        {
           conn = new SqlConnection("Data Source=MCONSTANTINESCU; Initial Catalog = Theatre; Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = True; ApplicationIntent = ReadWrite; MultiSubnetFailover = False");
        }

        public Show GetRecord(int id)
        {
            SqlCommand command = new SqlCommand("SELECT * FROM Shows WHERE [id]=@id", conn);
            command.Parameters.Add("@id", SqlDbType.Int);
            command.Parameters["@id"].Value = id;

            command.Connection = conn;
            conn.Open();

            using (var reader = command.ExecuteReader())
            {
                reader.Read();
                Show show = new Show
                {
                    Id = reader.GetInt32(reader.GetOrdinal("Id")),
                    Name = reader.GetString(reader.GetOrdinal("Name")),
                    Price = reader.GetInt32(reader.GetOrdinal("Price")),
                    AvailableTickets = reader.GetInt32(reader.GetOrdinal("AvailableTickets")),
                    DistributionList = reader.GetString(reader.GetOrdinal("DistributionList")),
                    Date = reader.GetDateTime(reader.GetOrdinal("Date"))
                };
                conn.Close();
                return show;
            }
        }

        public List<int> GetCategoryIdOfShow(int showId)
        {
            SqlCommand command = new SqlCommand("SELECT DISTINCT Categories.Id FROM Categories" +
                " INNER JOIN ShowCategories ON Categories.Id = ShowCategories.CategoryId" +
                " INNER JOIN Shows on ShowCategories.ShowId = @showId", conn);

            command.Parameters.Add("@showId", SqlDbType.Int);
            command.Parameters["@showId"].Value = showId;

            command.Connection = conn;
            conn.Open();
            var categoryIdList = new List<int>();

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    var id = reader.GetInt32(reader.GetOrdinal("Id"));

                    categoryIdList.Add(id);
                }
            }
            conn.Close();
            return categoryIdList;
        }

        public List<RowSeat> GetOccupiedSeatRowOfShow(int showId)
        {
            SqlCommand command = new SqlCommand("SELECT Row, Seat FROM ShowCashiers WHERE ShowId=@showId", conn);

            command.Parameters.Add("@showId", SqlDbType.Int);
            command.Parameters["@showId"].Value = showId;

            command.Connection = conn;
            conn.Open();

            var rowSeatList = new List<RowSeat>();

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    var rowSeat = new RowSeat
                    {
                        Row  = reader.GetString(reader.GetOrdinal("Row")),
                        Seat = reader.GetInt32(reader.GetOrdinal("Seat"))
                };

                    rowSeatList.Add(rowSeat);
                }
            }
            conn.Close();
            return rowSeatList;
        }

        public List<Show> GetRecords()
        {
            SqlConnection conn = new SqlConnection("Data Source=MCONSTANTINESCU; Initial Catalog = Theatre; Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = True; ApplicationIntent = ReadWrite; MultiSubnetFailover = False");
            SqlCommand command = new SqlCommand("SELECT * FROM Shows", conn);

            command.Connection = conn;
            conn.Open();
            var showList = new List<Show>();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    var show = new Show
                    {
                        Id = reader.GetInt32(reader.GetOrdinal("Id")),
                        Name = reader.GetString(reader.GetOrdinal("Name")),
                        Price = reader.GetInt32(reader.GetOrdinal("Price")),
                        AvailableTickets = reader.GetInt32(reader.GetOrdinal("AvailableTickets")),
                        DistributionList = reader.GetString(reader.GetOrdinal("DistributionList")),
                        Date = reader.GetDateTime(reader.GetOrdinal("Date"))
                    };
                    showList.Add(show);
                }
            }
            conn.Close();
            return showList;
        }

        public void DeleteRecord(int id)
        {
            SqlCommand command = new SqlCommand("DELETE FROM Shows WHERE [id]=@id", conn);
            command.Parameters.Add("@id", SqlDbType.Int);
            command.Parameters["@id"].Value = id;

            command.Connection = conn;
            conn.Open();

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {

                }
            }
            conn.Close();
        }

        public void InsertRecord(Show show)
        {
            string sqlStatement = "INSERT INTO Shows (Name, Price, Date, AvailableTickets, DistributionList) VALUES " +
                "(@Name, @Price, @Date, @AvailableTickets, @DistributionList)";

            SqlCommand command = new SqlCommand(sqlStatement, conn);

            command.Parameters.Add("@Name", SqlDbType.NVarChar);
            command.Parameters["@Name"].Value = show.Name;

            command.Parameters.Add("@Price", SqlDbType.Int);
            command.Parameters["@Price"].Value = show.Price;

            command.Parameters.Add("@Date", SqlDbType.DateTime2);
            command.Parameters["@Date"].Value = show.Date;

            command.Parameters.Add("@AvailableTickets", SqlDbType.Int);
            command.Parameters["@AvailableTickets"].Value = show.AvailableTickets;

            command.Parameters.Add("@DistributionList", SqlDbType.NVarChar);
            command.Parameters["@DistributionList"].Value = show.DistributionList;

            command.Connection = conn;
            conn.Open();

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {

                }
            }
            conn.Close();
        }

        public void InsertCategoryToShow(int showId, int categoryId)
        {
            string sqlStatement = "INSERT INTO ShowCategories (ShowId, CategoryId) VALUES " +
               "(@ShowId, @CategoryId)";

            SqlCommand command = new SqlCommand(sqlStatement, conn);

            command.Parameters.Add("@ShowId", SqlDbType.Int);
            command.Parameters["@ShowId"].Value = showId;

            command.Parameters.Add("@CategoryId", SqlDbType.Int);
            command.Parameters["@CategoryId"].Value = categoryId;

            command.Connection = conn;
            conn.Open();

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {

                }
            }
            conn.Close();
        }

        public void UpdateRecord(Show show)
        {
            string sqlStatement = "UPDATE Shows SET Name = @Name, Price = @Price, AvailableTickets = @AvailableTickets, " +
                "DistributionList = @DistributionList, Date = @Date WHERE Id = @Id";

            SqlCommand command = new SqlCommand(sqlStatement, conn);

            command.Parameters.Add("@Id", SqlDbType.Int);
            command.Parameters["@Id"].Value = show.Id;

            command.Parameters.Add("@Name", SqlDbType.NVarChar);
            command.Parameters["@Name"].Value = show.Name;

            command.Parameters.Add("@Price", SqlDbType.Int);
            command.Parameters["@Price"].Value = show.Price;

            command.Parameters.Add("@Date", SqlDbType.DateTime);
            command.Parameters["@Date"].Value = show.Date;

            command.Parameters.Add("@AvailableTickets", SqlDbType.Int);
            command.Parameters["@AvailableTickets"].Value = show.AvailableTickets;

            command.Parameters.Add("@DistributionList", SqlDbType.NVarChar);
            command.Parameters["@DistributionList"].Value = show.DistributionList;

            command.Connection = conn;
            conn.Open();

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {

                }
            }
            conn.Close();
        }

        public void UpdateShowCategoryRecord(int showId, int categoryId)
        {
            string sqlStatement = "UPDATE ShowCategories SET CategoryID = @CategoryId, ShowId = @ShowId";
            SqlCommand command = new SqlCommand(sqlStatement, conn);

            command.Parameters.Add("@CategoryId", SqlDbType.Int);
            command.Parameters["@CategoryId"].Value = categoryId;

            command.Parameters.Add("@ShowId", SqlDbType.Int);
            command.Parameters["@ShowId"].Value = showId;

            command.Connection = conn;
            conn.Open();

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {

                }
            }
            conn.Close();
        }

        public void DeleteShowCategoryRecord(int showId)
        {
           SqlCommand command = new SqlCommand("DELETE FROM ShowCategories WHERE ShowId = @showId", conn);
            command.Parameters.Add("@showId", SqlDbType.Int);
            command.Parameters["@showId"].Value = showId;

            command.Connection = conn;
            conn.Open();

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                }
            }
            conn.Close();
        }

        public int GetIdOfShow(string name)
        {
            SqlCommand command = new SqlCommand("SELECT Id FROM Shows WHERE [Name]=@Name", conn);
            command.Parameters.Add("@Name", SqlDbType.NVarChar);
            command.Parameters["@Name"].Value = name;
            command.Connection = conn;

            conn.Open();
            var showId = 0;

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    showId = reader.GetInt32(reader.GetOrdinal("Id"));
                }
            }
            conn.Close();
            return showId;
        }
    }
}
