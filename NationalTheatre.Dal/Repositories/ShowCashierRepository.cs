﻿using NationalTheatre.Dal.IRepositories;
using NationalTheatre.Dal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace NationalTheatre.Dal.Repositories
{
    public class ShowCashierRepository : IShowCashierRepository
    {
        SqlConnection conn;

        public ShowCashierRepository()
        {
            conn = new SqlConnection("Data Source=MCONSTANTINESCU; Initial Catalog = Theatre; Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = True; ApplicationIntent = ReadWrite; MultiSubnetFailover = False");
        }

        public void ReserveRowSeat(int cashierId, int showId, RowSeat rowSeat, DateTime date)
        {
           string sqlStatement = "INSERT INTO ShowCashiers (ShowId, CashierId, Row, Seat, ReservationDate) " +
                "VALUES (@showId, @cashierId, @Row, @Seat, @Date)";
            SqlCommand command = new SqlCommand(sqlStatement, conn);

            command.Parameters.Add("@showId", SqlDbType.Int);
            command.Parameters["@showId"].Value = showId;

            command.Parameters.Add("@cashierId", SqlDbType.Int);
            command.Parameters["@cashierId"].Value = cashierId;

            command.Parameters.Add("@Row", SqlDbType.NVarChar);
            command.Parameters["@Row"].Value = rowSeat.Row;

            command.Parameters.Add("@Seat", SqlDbType.Int);
            command.Parameters["@Seat"].Value = rowSeat.Seat;

            command.Parameters.Add("@Date", SqlDbType.DateTime);
            command.Parameters["@Date"].Value = date;

            command.Connection = conn;
            conn.Open();

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {

                }
            }
            conn.Close();
        }

        public List<Reservation> GetReservationsOfShow(int showId)
        {
            SqlCommand command = new SqlCommand("SELECT ShowCashiers.Id, ShowCashiers.Row, ShowCashiers.Seat, " +
                "ShowCashiers.ReservationDate," +
                "Shows.Name FROM ShowCashiers JOIN Shows on Shows.Id = ShowCashiers.ShowId " +
                "WHERE ShowCashiers.ShowId = @showId", conn);
            
            command.Parameters.Add("@showId", SqlDbType.Int);
            command.Parameters["@showId"].Value = showId;

            command.Connection = conn;
            conn.Open();

            List<Reservation> reservations = new List<Reservation>();

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    var rowSeat = new RowSeat
                    {
                        Row = reader.GetString(reader.GetOrdinal("Row")),
                        Seat = reader.GetInt32(reader.GetOrdinal("Seat"))
                    };

                    var reservation = new Reservation
                    {
                        Id = reader.GetInt32(reader.GetOrdinal("Id")),
                        RowSeat = rowSeat,
                        Date = reader.GetDateTime(reader.GetOrdinal("ReservationDate")),
                        Show = reader.GetString(reader.GetOrdinal("Name")),
                    };
                    reservations.Add(reservation);
                }
            }
            conn.Close();
            return reservations;
        }

        public Reservation GetReservation(int id)
        {
            SqlCommand command = new SqlCommand("SELECT ShowCashiers.Row, ShowCashiers.Seat, " +
                "ShowCashiers.ReservationDate,ShowCashiers.Id, ShowCashiers.ShowId, " +
                "Shows.Name FROM ShowCashiers JOIN Shows on Shows.Id = ShowCashiers.ShowId " +
                "WHERE ShowCashiers.Id = @reservationId", conn);

            command.Parameters.Add("@reservationId", SqlDbType.Int);
            command.Parameters["@reservationId"].Value = id;

            command.Connection = conn;
            conn.Open();
            var reservation = new Reservation();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    var rowSeat = new RowSeat
                    {
                        Row = reader.GetString(reader.GetOrdinal("Row")),
                        Seat = reader.GetInt32(reader.GetOrdinal("Seat"))
                    };

                    reservation = new Reservation
                    {
                        Id = reader.GetInt32(reader.GetOrdinal("Id")),
                        ShowId = reader.GetInt32(reader.GetOrdinal("ShowId")),
                        RowSeat = rowSeat,
                        Date = reader.GetDateTime(reader.GetOrdinal("ReservationDate")),
                        Show = reader.GetString(reader.GetOrdinal("Name")),
                    };                   
                }
            }
            conn.Close();
            return reservation;
        }

        public void UpdateRowSeat(int reservationId, RowSeat rowSeat, RowSeat chosenRowSeat)
        {
            string sqlStatement = "UPDATE ShowCashiers SET Row = @NewRow, Seat = @NewSeat WHERE Id = @reservationId";

            SqlCommand command = new SqlCommand(sqlStatement, conn);

            command.Parameters.Add("@NewRow", SqlDbType.NVarChar);
            command.Parameters["@NewRow"].Value = chosenRowSeat.Row;

            command.Parameters.Add("@NewSeat", SqlDbType.Int);
            command.Parameters["@NewSeat"].Value = chosenRowSeat.Seat;

            command.Parameters.Add("@reservationId", SqlDbType.Int);
            command.Parameters["@reservationId"].Value = reservationId;

            command.Connection = conn;
            conn.Open();

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {

                }
            }
            conn.Close();
        }

        public void DeleteReservation(int id)
        {
            SqlCommand command = new SqlCommand("DELETE FROM ShowCashiers WHERE [Id]=@id", conn);

            command.Parameters.Add("@id", SqlDbType.Int);
            command.Parameters["@id"].Value = id;

            command.Connection = conn;
            conn.Open();

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {

                }
            }
            conn.Close();
        }
    }
}
