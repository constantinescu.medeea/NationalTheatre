﻿using NationalTheatre.Dal.IRepositories;
using NationalTheatre.Dal.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Data;

namespace NationalTheatre.Dal.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        SqlConnection conn;

        public CategoryRepository()
        {
            conn = new SqlConnection("Data Source=MCONSTANTINESCU; Initial Catalog = Theatre; Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = True; ApplicationIntent = ReadWrite; MultiSubnetFailover = False");
        }

        public Category GetRecord(int id)
        {
            SqlCommand command = new SqlCommand("SELECT * FROM Categories WHERE [id]=@id", conn);
            command.Parameters.Add("@id", SqlDbType.Int);
            command.Parameters["@id"].Value = id;

            command.Connection = conn;
            conn.Open();

            using (var reader = command.ExecuteReader())
            {
                reader.Read();
                Category category = new Category
                {
                    Id = reader.GetInt32(reader.GetOrdinal("Id")),
                    Name = reader.GetString(reader.GetOrdinal("Name"))                   
                };
                conn.Close();
                return category;
            }
        }

        public List<Category> GetRecords()
        {
            SqlCommand command = new SqlCommand("SELECT * FROM Categories", conn);

            command.Connection = conn;
            conn.Open();
            var categoryList = new List<Category>();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    var category = new Category
                    {
                        Id = reader.GetInt32(reader.GetOrdinal("Id")),
                        Name = reader.GetString(reader.GetOrdinal("Name"))                       
                    };
                    categoryList.Add(category);
                }
            }
            conn.Close();
            return categoryList;
        }

        public void DeleteRecord(int id)
        {
            SqlCommand command = new SqlCommand("DELETE FROM Categories WHERE [id]=@id", conn);
            command.Parameters.Add("@id", SqlDbType.Int);
            command.Parameters["@id"].Value = id;

            command.Connection = conn;
            conn.Open();

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {

                }
            }
            conn.Close();
        }

        public void InsertRecord(Category category)
        {
            string sqlStatement = "INSERT INTO Categories (Name) VALUES (@Name)";
            SqlCommand command = new SqlCommand(sqlStatement, conn);

            command.Parameters.Add("@Name", SqlDbType.NVarChar);
            command.Parameters["@Name"].Value = category.Name;

            command.Connection = conn;
            conn.Open();

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {

                }
            }
            conn.Close();
        }

        public void UpdateRecord(Category category)
        {
            string sqlStatement = "UPDATE Categories SET Name = @Name WHERE Id = @Id";
            SqlCommand command = new SqlCommand(sqlStatement, conn);

            command.Parameters.Add("@Id", SqlDbType.Int);
            command.Parameters["@Id"].Value = category.Id;

            command.Parameters.Add("@Name", SqlDbType.NVarChar);
            command.Parameters["@Name"].Value = category.Name;

            command.Connection = conn;
            conn.Open();

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {

                }
            }
            conn.Close();
        }
    }
}
