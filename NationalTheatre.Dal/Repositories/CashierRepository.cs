﻿using NationalTheatre.Dal.IRepositories;
using NationalTheatre.Dal.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Data;

namespace NationalTheatre.Dal.Repositories
{
    public class CashierRepository : ICashierRepository
    {
        SqlConnection conn;

        public CashierRepository()
        {
            conn = new SqlConnection("Data Source=MCONSTANTINESCU; Initial Catalog = Theatre; Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = True; ApplicationIntent = ReadWrite; MultiSubnetFailover = False");
        }

        public Cashier GetRecord(int id)
        {
            SqlCommand command = new SqlCommand("SELECT * FROM Cashiers WHERE [id]=@id", conn);
            command.Parameters.Add("@id", SqlDbType.Int);
            command.Parameters["@id"].Value = id;

            command.Connection = conn;
            conn.Open();

            using (var reader = command.ExecuteReader())
            {
                reader.Read();
                Cashier cashier = new Cashier
                {
                    Id = reader.GetInt32(reader.GetOrdinal("Id")),
                    FirstName = reader.GetString(reader.GetOrdinal("FirstName")),
                    LastName = reader.GetString(reader.GetOrdinal("LastName")),
                    Username = reader.GetString(reader.GetOrdinal("Username")),
                    Password = reader.GetString(reader.GetOrdinal("Password")),
                    Role = reader.GetString(reader.GetOrdinal("Role"))
                };
                conn.Close();
                return cashier;
            }
        }

        public int CountRecords(string username)
        {
            SqlCommand command = new SqlCommand("SELECT COUNT(*) FROM Cashiers WHERE [Username]=@username", conn);
          
            command.Parameters.Add("@username", SqlDbType.NVarChar);
            command.Parameters["@username"].Value = username;
            command.Connection = conn;
            conn.Open();
            var count = Convert.ToInt32(command.ExecuteScalar());
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    
                }
            }
            conn.Close();
            return count;           
        }

        public List<Cashier> GetRecords()
        {
            SqlCommand command = new SqlCommand("SELECT * FROM Cashiers WHERE Role=@Role", conn);
            command.Parameters.Add("@Role", SqlDbType.NVarChar);
            command.Parameters["@Role"].Value = "cashier";

            command.Connection = conn;
            conn.Open();
            var cashierList = new List<Cashier>();

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    var cashier = new Cashier
                    {
                        Id = reader.GetInt32(reader.GetOrdinal("Id")),
                        FirstName = reader.GetString(reader.GetOrdinal("FirstName")),
                        LastName = reader.GetString(reader.GetOrdinal("LastName")),
                        Username = reader.GetString(reader.GetOrdinal("Username")),
                        Password = reader.GetString(reader.GetOrdinal("Password")),
                        Role = reader.GetString(reader.GetOrdinal("Role"))
                    };
                    cashierList.Add(cashier);
                }
            }
            conn.Close();
            return cashierList;
        }

        public void DeleteRecord(int id)
        {
            SqlCommand command = new SqlCommand("DELETE FROM Cashiers WHERE [id]=@id", conn);
            command.Parameters.Add("@id", SqlDbType.Int);
            command.Parameters["@id"].Value = id;


            command.Connection = conn;
            conn.Open();

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {

                }
            }
            conn.Close();
        }

        public void InsertRecord(Cashier cashier)
        {
            string sqlStatement = "INSERT INTO Cashiers (FirstName, LastName, Username, Password, Role) " +
                "VALUES (@FirstName, @LastName, @Username, @Password, @Role)";
            SqlCommand command = new SqlCommand(sqlStatement, conn);

            command.Parameters.Add("@FirstName", SqlDbType.NVarChar);
            command.Parameters["@FirstName"].Value = cashier.FirstName;

            command.Parameters.Add("@LastName", SqlDbType.NVarChar);
            command.Parameters["@LastName"].Value = cashier.LastName;

            command.Parameters.Add("@Username", SqlDbType.NVarChar);
            command.Parameters["@Username"].Value = cashier.Username;

            command.Parameters.Add("@Password", SqlDbType.NVarChar);
            command.Parameters["@Password"].Value = cashier.Password;

            command.Parameters.Add("@Role", SqlDbType.NVarChar);
            command.Parameters["@Role"].Value = cashier.Role;

            command.Connection = conn;
            conn.Open();

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {

                }
            }
            conn.Close();
        }

        public void UpdateRecord(Cashier cashier)
        {
            string sqlStatement = "UPDATE Cashiers SET FirstName = @FirstName, LastName = @LastName, " +
                " Username = @Username, Password = @Password WHERE Id = @Id";
            SqlCommand command = new SqlCommand(sqlStatement, conn);

            command.Parameters.Add("@Id", SqlDbType.Int);
            command.Parameters["@Id"].Value = cashier.Id;

            command.Parameters.Add("@FirstName", SqlDbType.NVarChar);
            command.Parameters["@FirstName"].Value = cashier.FirstName;

            command.Parameters.Add("@LastName", SqlDbType.NVarChar);
            command.Parameters["@LastName"].Value = cashier.LastName;

            command.Parameters.Add("@Username", SqlDbType.NVarChar);
            command.Parameters["@Username"].Value = cashier.Username;

            command.Parameters.Add("@Password", SqlDbType.NVarChar);
            command.Parameters["@Password"].Value = cashier.Password;

            command.Connection = conn;
            conn.Open(); 

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {

                }
            }
            conn.Close();
        }

        public Cashier GetCashierByUsername(string username)
        {
            SqlCommand command = new SqlCommand("SELECT * FROM Cashiers WHERE [Username]=@username", conn);
            command.Parameters.Add("@username", SqlDbType.NVarChar);
            command.Parameters["@username"].Value = username;

            command.Connection = conn;
            conn.Open();

            using (var reader = command.ExecuteReader())
            {
                reader.Read();
                Cashier cashier = new Cashier
                {
                    Id = reader.GetInt32(reader.GetOrdinal("Id")),
                    FirstName = reader.GetString(reader.GetOrdinal("FirstName")),
                    LastName = reader.GetString(reader.GetOrdinal("LastName")),
                    Username = reader.GetString(reader.GetOrdinal("Username")),
                    Password = reader.GetString(reader.GetOrdinal("Password")),
                    Role = reader.GetString(reader.GetOrdinal("Role"))
                };
                conn.Close();
                return cashier;
            }
        }
    }
}
