﻿using NationalTheatre.Dal.IRepositories;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace NationalTheatre.Dal.Repositories
{
    public class RowRepository : IRowRepository
    {
        SqlConnection conn;

        public RowRepository()
        {
            conn = new SqlConnection("Data Source=MCONSTANTINESCU; Initial Catalog = Theatre; Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = True; ApplicationIntent = ReadWrite; MultiSubnetFailover = False");
        }

        public List<string> GetAllRows()
        {            
            SqlCommand command = new SqlCommand("SELECT Nr FROM Rows", conn);

            command.Connection = conn;
            conn.Open();
            var rowList = new List<string>();

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    var row = reader.GetString(reader.GetOrdinal("Nr"));

                    rowList.Add(row);
                }
            }
            conn.Close();
            return rowList;
        }
    }
    }

