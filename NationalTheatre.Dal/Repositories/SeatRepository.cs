﻿using NationalTheatre.Dal.IRepositories;
using NationalTheatre.Dal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace NationalTheatre.Dal.Repositories
{
    public class SeatRepository : ISeatRepository
    {
        SqlConnection conn;

        public SeatRepository()
        {
           conn = new SqlConnection("Data Source=MCONSTANTINESCU; Initial Catalog = Theatre; Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = True; ApplicationIntent = ReadWrite; MultiSubnetFailover = False");
        }

        public List<int> GetAllSeats()
        {
            SqlCommand command = new SqlCommand("SELECT Nr FROM Seats", conn);

            command.Connection = conn;
            conn.Open();
            var seatList = new List<int>();

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    var seat = reader.GetInt32(reader.GetOrdinal("Nr"));
                   
                    seatList.Add(seat);
                }
            }
            conn.Close();
            return seatList;
        }
    }
}
