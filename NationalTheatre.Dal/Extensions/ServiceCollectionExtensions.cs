﻿using Microsoft.Extensions.DependencyInjection;
using NationalTheatre.Dal.IRepositories;
using NationalTheatre.Dal.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.Dal.Extensions
{
    public  static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddNationalTheatreDal(this IServiceCollection services, string connectionString)
        {
            services.AddScoped<IShowRepository, ShowRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<ICashierRepository, CashierRepository>();
            services.AddScoped<IRowRepository, RowRepository>();
            services.AddScoped<ISeatRepository, SeatRepository>();
            services.AddScoped<IShowCashierRepository, ShowCashierRepository>();

            return services;
        }
    }
}
