﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using NationalTheatre.Models;

namespace NationalTheatre.BusinessLogic.Export
{
    public class ExportXml : IExportFile
    {
        public void Export(List<ReservationModel> reservationModelList, string showName)
        {
            var exportDate = string.Format("text-{0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now);
            var path = "wwwroot/files/" + showName.Replace(" ", string.Empty) + "_"
                + exportDate + ".xml";
             using (XmlWriter writer = XmlWriter.Create(path))
                {
                    writer.WriteStartDocument();
                    writer.WriteStartElement("Reservations");
                if (reservationModelList.Count > 0)
                {

                    foreach (var reservation in reservationModelList)
                    {
                        writer.WriteStartElement("Reservation");
                        writer.WriteElementString("Show", reservation.Show);
                        writer.WriteElementString("Row", reservation.RowSeat.Row);
                        writer.WriteElementString("Seat", reservation.RowSeat.Seat.ToString());
                        writer.WriteElementString("ReservationDate", reservation.Date.ToString());
                        writer.WriteEndElement();
                    }
                }
                else
                {
                    writer.WriteStartElement("Reservations");
                    writer.WriteElementString("Content", "No reservations were made");                   
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }
    }
}
