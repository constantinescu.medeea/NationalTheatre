﻿using NationalTheatre.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;

namespace NationalTheatre.BusinessLogic.Export
{
    public class ExportCsv : IExportFile
    {
        public void Export(List<ReservationModel> reservationModelList, string showName)
        {
            var  csv = new StringBuilder();
            if (reservationModelList.Count > 0)
            {
                foreach (var reservation in reservationModelList)
                {
                    var show = reservation.Show;
                    var rowSeat = reservation.RowSeat.Row + " " + reservation.RowSeat.Seat.ToString();
                    var date = reservation.Date.ToString();

                    var newLine = string.Format("{0}, {1}, {2}", show, rowSeat, date);
                    csv.AppendLine(newLine);

                }
            }
            else
            {
                var text = "No reservations were made";
                var newLine = string.Format("{0}", text);
                csv.AppendLine(newLine);
            }

            var exportDate = string.Format("text-{0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now);
            var path = "wwwroot/files/" + showName.Replace(" ", string.Empty) +"_"
                + exportDate + ".csv";

            File.WriteAllText(path, csv.ToString());
        }
    }
} 
