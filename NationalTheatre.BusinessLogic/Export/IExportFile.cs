﻿using NationalTheatre.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.BusinessLogic.Export
{
    public interface IExportFile
    {
        void Export(List<ReservationModel> reservationModelList, string showName);
    }
}
