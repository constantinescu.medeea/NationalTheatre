﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.BusinessLogic.Export
{
    public class ExportFileFactory : ExportFactory
    {
        public override IExportFile GetFile(string File)
        {
            switch (File)
            {
                case "csv":
                    return new ExportCsv();
                case "xml":
                    return new ExportXml();
                default:
                    throw new ApplicationException(string.Format("File '{0}' cannot be created", File));
            }
        }
    }
}
