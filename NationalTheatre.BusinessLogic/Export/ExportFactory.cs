﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.BusinessLogic.Export
{
    public abstract class ExportFactory
    {
        public abstract IExportFile GetFile(string File);
    }
}
