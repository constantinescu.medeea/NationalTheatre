﻿using NationalTheatre.BusinessLogic.Converters;
using NationalTheatre.BusinessLogic.ILogic;
using NationalTheatre.Dal.IRepositories;
using NationalTheatre.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace NationalTheatre.BusinessLogic.Logic
{
    public class CategoryLogic : ICategoryLogic
    {
        private readonly ICategoryRepository _iCategoryRepository;
        
        public CategoryLogic(ICategoryRepository iCategoryRepository)
        {
            _iCategoryRepository = iCategoryRepository;
        }

        public List<CategoryModel> GetCategories()
        {
            var categoryList = _iCategoryRepository.GetRecords();

            var categoryModel = categoryList.Select(c => c.ToCategoryModel()).ToList();

            return categoryModel;
        }

        public CategoryModel GetById(int id)
        {            
            var category = _iCategoryRepository.GetRecord(id);
            var categoryModel = category.ToCategoryModel();
            return categoryModel;
        }

        public void DeleteCategory(int id)
        {           
            _iCategoryRepository.DeleteRecord(id);
        }

        public void CreateCategory(CategoryModel categoryModel)
        {
            var category = categoryModel.ToCategory();

            _iCategoryRepository.InsertRecord(category);
        }

        public void UpdateCategory(CategoryModel categoryModel)
        {
            var category = categoryModel.ToCategory();

            _iCategoryRepository.UpdateRecord(category);
        }
    }
}
