﻿using NationalTheatre.BusinessLogic.Converters;
using NationalTheatre.BusinessLogic.ILogic;
using NationalTheatre.Dal.IRepositories;
using NationalTheatre.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NationalTheatre.BusinessLogic.Logic
{
    public class ShowLogic : IShowLogic
    {
        private readonly IShowRepository _iShowRepository;
        private readonly ISeatRepository _iSeatRepository;
        private readonly IRowRepository _iRowRepository;

        public ShowLogic(IShowRepository iShowRepository, ISeatRepository iSeatRepository,
            IRowRepository iRowRepository)
        {
            _iShowRepository = iShowRepository;
            _iSeatRepository = iSeatRepository;
            _iRowRepository = iRowRepository;
        }

        public ShowLogic(IShowRepository iShowRepository)
        {
            _iShowRepository = iShowRepository;
        }

        public bool SellTickets(ShowModel showModel, int nrOfTicketsToSell)
        {
            if (showModel.AvailableTickets < nrOfTicketsToSell)
                return false;
            return true;
        }

        public List<ShowModel> GetShows()
        {
            var list = _iShowRepository.GetRecords();
            var showModel = list.Select(s => s.ToShowModel()).ToList();

            return showModel;
        }

        public List<ShowModel> GetCurrentShows()
        {
            var list = _iShowRepository.GetRecords();
            var showModel = list.Select(s => s.ToShowModel()).Where(d => d.Date >= DateTime.Now).ToList();

            return showModel;
        }

        public ShowModel GetById(int id)
        {
            var show = _iShowRepository.GetRecord(id);
            var showModel = show.ToShowModel();
            return showModel;
        }

        public int GetIdOfShow(string name)
        {
            var showId = _iShowRepository.GetIdOfShow(name);

            return showId;
        }

        public List<int> GetCategoriesOfShow(int showId)
        {
            var categoryIdList = _iShowRepository.GetCategoryIdOfShow(showId);

            return categoryIdList;
        }

        public void DeleteShow(int id)
        {
            _iShowRepository.DeleteRecord(id);
        }

        public void CreateShow(ShowModel showModel)
        {
            var show = showModel.ToShow();

            _iShowRepository.InsertRecord(show);
        }

        public void AddCategoriesToShow(List<int> selectedCategories, int showId)
        {
            foreach (var categoryId in selectedCategories)
            {
                _iShowRepository.InsertCategoryToShow(showId, categoryId);
            }
        }

        public void UpdateShow(ShowModel showModel)
        {
            var show = showModel.ToShow();

            _iShowRepository.UpdateRecord(show);
        }

        public void UpdateShowCategory(ShowModel showModel)
        {
            var showId = showModel.Id;

            foreach (var category in showModel.SelectedCategories)
            {
                _iShowRepository.UpdateShowCategoryRecord(showId, category);
            }
        }

        public void DeleteShowCategories(ShowModel showModel)
        {
            var showId = showModel.Id;

            _iShowRepository.DeleteShowCategoryRecord(showId);
        }

        public bool VerifyReservation(int id)
        {
            var nrOfAvailableTickets = _iShowRepository.GetRecord(id).AvailableTickets;

            if (nrOfAvailableTickets > 0)
            {
                return true;
            }

            return false;
        }

        public List<RowSeatModel> GetOccupiedRowSeatOfShow(int showId)
        {
            return _iShowRepository.GetOccupiedSeatRowOfShow(showId).Select(r => r.ToRowSeatModel()).ToList();
        }

        public List<RowSeatModel> GetAvailableRowSeatList(int id)
        {
            var allSeats = _iSeatRepository.GetAllSeats();
            var allRows = _iRowRepository.GetAllRows();

            var occupiedRowSeats = GetOccupiedRowSeatOfShow(id);

            var allRowSeats = new List<RowSeatModel>();
            var availableSeats = new List<RowSeatModel>();
            var i = 0;
            foreach (var seat in allSeats)
            {
                foreach (var row in allRows)
                {
                    var rowSeat = new RowSeatModel
                    {
                        Row = row,
                        Seat = seat,
                        Id = i
                    };
                    i++;
                    allRowSeats.Add(rowSeat);
                }
            }           

            foreach (var occupied in occupiedRowSeats)
            {
                var delete = false;
                var ind = 0;
                foreach (var rowSeat in allRowSeats)
                {
                    if (occupied.Row.Equals(rowSeat.Row) && occupied.Seat == rowSeat.Seat)
                    {
                        ind = allRowSeats.IndexOf(rowSeat);
                        delete = true;
                        break;
                    }                   

                }
                if (delete)
                {
                    allRowSeats.RemoveAt(ind);
                }
            }

            return allRowSeats;
        }

    }
}
