﻿using NationalTheatre.BusinessLogic.Converters;
using NationalTheatre.BusinessLogic.ILogic;
using NationalTheatre.Dal.IRepositories;
using NationalTheatre.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace NationalTheatre.BusinessLogic.Logic
{
    public class CashierLogic : ICashierLogic
    {
        private readonly ICashierRepository _iCashierRepository;

        public CashierLogic(ICashierRepository iCashierRepository)
        {
            _iCashierRepository = iCashierRepository;
        }

        public bool MethodTotest(string username, string password)
        {
            var cashier = _iCashierRepository.GetCashierByUsername(username);
            if (cashier.Password.Equals(password))
                return true;
            return false;
        }

        public List<CashierModel> GetCashiers()
        {       
            var cashierList = _iCashierRepository.GetRecords();
            var cashierModel = cashierList.Select(c => c.ToCashierModel()).ToList();

            return cashierModel;
        }

        public CashierModel GetById(int id)
        {
            var cashier = _iCashierRepository.GetRecord(id);
            var cashierModel = cashier.ToCashierModel();
            return cashierModel;
        }

        public void DeleteCashier(int id)
        {
            _iCashierRepository.DeleteRecord(id);
        }

        public void CreateCashier(CashierModel cashierModel)
        {
            var cashier = cashierModel.ToCashier();

            _iCashierRepository.InsertRecord(cashier);
        }

        public void UpdateCashier(CashierModel cashierModel)
        {
            var cashier = cashierModel.ToCashier();

            _iCashierRepository.UpdateRecord(cashier);
        }

        public bool VerifyUser(string username, string password)
        {
            if (password == GetUserByUsername(username).Password)
            {
                return true;
            }
            return false;
        }

        public CashierModel GetUserByUsername(string username)
        {
            var cashierModel = _iCashierRepository.GetCashierByUsername(username).ToCashierModel();
            return cashierModel;
        }

        public int CountRecords(string username)
        {
            return _iCashierRepository.CountRecords(username);
        }

        public bool VerifyUsername(string username)
        {
            if(_iCashierRepository.CountRecords(username) == 1)
            {
                return true;
            }
            return false;
        }
    }
}
