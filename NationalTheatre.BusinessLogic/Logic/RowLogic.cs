﻿using NationalTheatre.BusinessLogic.ILogic;
using NationalTheatre.Dal.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.BusinessLogic.Logic
{
    public class RowLogic : IRowLogic
    {
        public IRowRepository _iRowRepository;

        public RowLogic(IRowRepository iRowRepository)
        {
            _iRowRepository = iRowRepository;
        }

        public List<string> GetAllRows()
        {
            return _iRowRepository.GetAllRows();
        }
    }
}
