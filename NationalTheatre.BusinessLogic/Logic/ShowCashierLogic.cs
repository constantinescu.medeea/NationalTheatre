﻿using NationalTheatre.BusinessLogic.Converters;
using NationalTheatre.BusinessLogic.ILogic;
using NationalTheatre.Dal.IRepositories;
using NationalTheatre.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NationalTheatre.BusinessLogic.Logic
{
    public class ShowCashierLogic : IShowCashierLogic
    {
        private readonly ICashierRepository _iCashierRepository;
        private readonly IShowCashierRepository _iShowCashierRepository;
        private readonly IShowRepository _iShowRepository;

        public ShowCashierLogic(ICashierRepository iCashierRepository, 
            IShowCashierRepository iShowCashierRepository, IShowRepository iShowRepository)
        {
            _iCashierRepository = iCashierRepository;
            _iShowCashierRepository = iShowCashierRepository;
            _iShowRepository = iShowRepository;
        }

        public void ReserveRowSeat(string username, int showId, List<RowSeatModel> rowSeatModelList, DateTime date)
        {
            var cashierId = _iCashierRepository.GetCashierByUsername(username).Id;
            var rowSeatList = rowSeatModelList.Select(r => r.ToRowSeat()).ToList();

            var nrOfTickets = rowSeatModelList.Count();
            var show = _iShowRepository.GetRecord(showId);
            show.AvailableTickets = show.AvailableTickets - nrOfTickets;

            _iShowRepository.UpdateRecord(show);

            foreach (var rowSeat in rowSeatList)
            {
                _iShowCashierRepository.ReserveRowSeat(cashierId, showId, rowSeat, date);
            }            
        }

        public List<ReservationModel> ViewReservations(int showId)
        {
            return _iShowCashierRepository.GetReservationsOfShow(showId)
                .Select(s => s.ToReservationModel()).ToList();
        }

        public ReservationModel GetReservation(int id)
        {
            return _iShowCashierRepository.GetReservation(id).ToReservationModel();
        }

        public void UpdateRowSeat(int reservationId, RowSeatModel rowSeat, RowSeatModel chosenRowSeat)
        {
            _iShowCashierRepository.UpdateRowSeat(reservationId, rowSeat.ToRowSeat(), chosenRowSeat.ToRowSeat());
        }


        public void DeleteReservation(int reservationId, int showId)
        {
            _iShowCashierRepository.DeleteReservation(reservationId);

            var show = _iShowRepository.GetRecord(showId);
            show.AvailableTickets = show.AvailableTickets + 1;

            _iShowRepository.UpdateRecord(show);
        }
    }
}
