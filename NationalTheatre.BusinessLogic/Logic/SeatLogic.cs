﻿using NationalTheatre.BusinessLogic.ILogic;
using NationalTheatre.Dal.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.BusinessLogic.Logic
{
    public class SeatLogic : ISeatLogic
    {
        public ISeatRepository _iSeatRepository;

        public SeatLogic(ISeatRepository iSeatRepository)
        {
            _iSeatRepository = iSeatRepository;
        }

        public List<int> GetAllSeats()
        {
            return _iSeatRepository.GetAllSeats();
        }
    }
}
