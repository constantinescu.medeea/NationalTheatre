﻿using Microsoft.Extensions.DependencyInjection;
using NationalTheatre.BusinessLogic.ILogic;
using NationalTheatre.BusinessLogic.Logic;
using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.BusinessLogic.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddNationalTheatreBusinessLogic(this IServiceCollection services)
        {
            services.AddScoped<IShowLogic, ShowLogic>();
            services.AddScoped<ICategoryLogic, CategoryLogic>();
            services.AddScoped<ICashierLogic, CashierLogic>();
            services.AddScoped<IRowLogic, RowLogic>();
            services.AddScoped<ISeatLogic, SeatLogic>();
            services.AddScoped<IShowCashierLogic, ShowCashierLogic>();

            return services;
        }
    }
}
