﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.BusinessLogic.ILogic
{
    public interface IRowLogic
    {
        List<string> GetAllRows();
    }
}
