﻿using NationalTheatre.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.BusinessLogic.ILogic
{
    public interface ICategoryLogic
    {
        List<CategoryModel> GetCategories();

        void DeleteCategory(int id);

        CategoryModel GetById(int id);

        void CreateCategory(CategoryModel categoryModel);

        void UpdateCategory(CategoryModel categoryModel);
    }
}
