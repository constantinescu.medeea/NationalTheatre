﻿using NationalTheatre.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.BusinessLogic.ILogic
{
    public interface IShowCashierLogic
    {
        void ReserveRowSeat(string username, int showId, List<RowSeatModel> rowSeatModelList, DateTime date);

        List<ReservationModel> ViewReservations(int showId);

        ReservationModel GetReservation(int id);

        void UpdateRowSeat(int reservationId, RowSeatModel rowSeat, RowSeatModel chosenRowSeat);

        void DeleteReservation(int reservationId, int showId);

    }
}
