﻿using NationalTheatre.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.BusinessLogic.ILogic
{
    public interface IShowLogic
    {
        List<ShowModel> GetShows();

        void DeleteShow(int id);

        ShowModel GetById(int id);

        void CreateShow(ShowModel showModel);

        void UpdateShow(ShowModel showModel);

        int GetIdOfShow(string Name);

        void AddCategoriesToShow(List<int> selectedCategories, int showId);

        List<int> GetCategoriesOfShow(int showId);

        void UpdateShowCategory(ShowModel showModel);

        void DeleteShowCategories(ShowModel showModel);

        bool VerifyReservation(int id);

        List<ShowModel> GetCurrentShows();

        List<RowSeatModel> GetOccupiedRowSeatOfShow(int showId);

        List<RowSeatModel> GetAvailableRowSeatList(int id);

    }
}
