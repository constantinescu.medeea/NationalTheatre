﻿using NationalTheatre.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.BusinessLogic.ILogic
{
    public interface ICashierLogic
    {
        List<CashierModel> GetCashiers();

        void DeleteCashier(int id);

        CashierModel GetById(int id);

        void CreateCashier(CashierModel cashierModel);

        void UpdateCashier(CashierModel cashierModel);

        bool VerifyUser(string username, string password);

        CashierModel GetUserByUsername(string username);

        int CountRecords(string username);

        bool VerifyUsername(string username);
    }
}
