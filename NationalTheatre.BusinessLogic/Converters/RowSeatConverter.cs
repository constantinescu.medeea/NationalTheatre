﻿using NationalTheatre.Dal.Models;
using NationalTheatre.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.BusinessLogic.Converters
{
    public static class RowSeatConverter
    {
        public static RowSeatModel ToRowSeatModel(this RowSeat rowSeat)
        {
            if (rowSeat == null)
            {
                return null;
            }

            RowSeatModel rowSeatModel = new RowSeatModel
            {
                Row = rowSeat.Row,
                Seat = rowSeat.Seat
            };

            return rowSeatModel;
        }

        public static RowSeat ToRowSeat(this RowSeatModel rowSeatModel)
        {
            if (rowSeatModel == null)
            {
                return null;
            }

            RowSeat rowSeat = new RowSeat
            {
                Row = rowSeatModel.Row,
                Seat = rowSeatModel.Seat
            };

            return rowSeat;
        }
    }
}
