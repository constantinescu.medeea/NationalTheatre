﻿using NationalTheatre.Dal.Models;
using NationalTheatre.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.BusinessLogic.Converters
{
    public static class ReservationConverter
    {
        public static ReservationModel ToReservationModel(this Reservation reservation)
        {
            if (reservation == null)
            {
                return null;
            }

            ReservationModel reservationModel = new ReservationModel
            {
                Id = reservation.Id,
                Date = reservation.Date,
                ShowId = reservation.ShowId,
                Show = reservation.Show,
                RowSeat = reservation.RowSeat.ToRowSeatModel()
            };

            return reservationModel;
        }

        public static Reservation ToReservation(this ReservationModel reservationModel)
        {
            if (reservationModel == null)
            {
                return null;
            }

            Reservation reservation = new Reservation
            {
                Id = reservationModel.Id,
                Date = reservationModel.Date,
                ShowId = reservationModel.ShowId,
                Show = reservationModel.Show,
                RowSeat = reservationModel.RowSeat.ToRowSeat()
            };

            return reservation;
        }
    }
}
