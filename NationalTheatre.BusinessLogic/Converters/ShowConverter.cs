﻿using NationalTheatre.Dal.Models;
using NationalTheatre.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.BusinessLogic.Converters
{
    public static class ShowConverter
    {
        public static ShowModel ToShowModel(this Show show)
        {
            if (show == null)
            {
                return null;
            }

            ShowModel showModel = new ShowModel
            {
                Id = show.Id,
                Name = show.Name,
                Price = show.Price,
                AvailableTickets = show.AvailableTickets,
                DistributionList = show.DistributionList,
                Date = show.Date,
                Categories = new List<CategoryModel>()
            };

            return showModel;
        }

        public static Show ToShow(this ShowModel showModel)
        {
            if (showModel == null)
            {
                return null;
            }

            Show show = new Show
            {
                Id = showModel.Id,
                Name = showModel.Name,
                Price = showModel.Price,
                Date = showModel.Date,
                AvailableTickets = showModel.AvailableTickets,
                DistributionList = showModel.DistributionList               
            };

            return show;
        }
    }
}
