﻿using NationalTheatre.Dal.Models;
using NationalTheatre.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.BusinessLogic.Converters
{
    public static class CashierConverter
    {
        public static CashierModel ToCashierModel(this Cashier cashier)
        {
            if (cashier == null)
            {
                return null;
            }

            CashierModel cashierModel = new CashierModel
            {
                Id = cashier.Id,
                FirstName = cashier.FirstName,
                LastName = cashier.LastName,
                Username = cashier.Username,
                Password = cashier.Password,
                Role = cashier.Role
            };

            return cashierModel;
        }

        public static Cashier ToCashier(this CashierModel cashierModel)
        {
            if (cashierModel == null)
            {
                return null;
            }

            Cashier cashier = new Cashier
            {
                Id = cashierModel.Id,
                FirstName = cashierModel.FirstName,
                LastName = cashierModel.LastName,
                Username = cashierModel.Username,
                Password = cashierModel.Password,
                Role = cashierModel.Role
            };

            return cashier;
        }
    }
}
