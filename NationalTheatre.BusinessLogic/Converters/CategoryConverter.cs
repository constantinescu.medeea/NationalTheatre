﻿using NationalTheatre.Dal.Models;
using NationalTheatre.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NationalTheatre.BusinessLogic.Converters
{
    public static class CategoryConverter
    {
        public static CategoryModel ToCategoryModel(this Category category)
        {
            if (category == null)
            {
                return null;
            }

            CategoryModel categoryModel = new CategoryModel
            {
                Id = category.Id,
                Name = category.Name                
            };

            return categoryModel;
        }

        public static Category ToCategory(this CategoryModel categoryModel)
        {
            if (categoryModel == null)
            {
                return null;
            }

            Category category = new Category
            {
                Id = categoryModel.Id,
                Name = categoryModel.Name
            };

            return category;
        }
    }
}
