using Microsoft.VisualStudio.TestTools.UnitTesting;
using NationalTheatre.BusinessLogic.Extensions;
using Moq;
using NationalTheatre.Dal.Models;
using NationalTheatre.Dal.IRepositories;
using NationalTheatre.BusinessLogic.Logic;
using NationalTheatre.BusinessLogic.Converters;

namespace NationalTheatre.UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        Mock<ICashierRepository> mockCashierRepository;
        CashierLogic cashierLogic;

        Mock<IShowRepository> mockShowRepository;
        ShowLogic showLogic;

        public UnitTest1()
        {
            mockCashierRepository = new Mock<ICashierRepository>();
            cashierLogic = new CashierLogic(mockCashierRepository.Object);

            mockShowRepository = new Mock<IShowRepository>();
            showLogic = new ShowLogic(mockShowRepository.Object);
        }

        [TestMethod]
        public void TestPasswordSuccess()
        {
            var cashier = new Cashier()
            {
                FirstName = "Medeea",
                LastName = "Constantinescu",
                Username = "medeea.constantinescu",
                Password = "medeea.constantinescu",
                Role = "cashier"
            };

            var newCashier = new Cashier();
            cashier.Password = cashier.Password.Encrypt();

            mockCashierRepository.Setup(x => x.GetCashierByUsername(cashier.Username)).Returns(cashier);
            var password = "aSAP6mmhmNqesJQ0dR9T0QG8aXSoXfDXY0pKoV/ehJ91bGpOpNE2q0Y7AXsLuNDC";
            
            var truthValue = cashierLogic.MethodTotest(cashier.Username, password);
            Assert.IsTrue(truthValue);
        }

        [TestMethod]
        public void TestPasswordFail()
        {
            var cashier = new Cashier()
            {
                FirstName = "Medeea",
                LastName = "Constantinescu",
                Username = "medeea.constantinescu",
                Password = "medeea.constantinescu",
                Role = "cashier"
            };

            var newCashier = new Cashier();
            cashier.Password = cashier.Password.Encrypt();

            mockCashierRepository.Setup(x => x.GetCashierByUsername(cashier.Username)).Returns(cashier);
            var password = "aSAP6mmhmNqesJQ0dR9T0QG8aXSoXfDXY0pKoV/ehJ91bGpOpNE2q0Y7AXsLuNDC1";
            
            var truthValue = cashierLogic.MethodTotest(cashier.Username, password);
            Assert.IsTrue(truthValue);
        }

        [TestMethod]
        public void SellTicketSuccess()
        {
            var show = new Show()
            {
                Id = 1,
                Name = "Straini in noapte",
                DistributionList = "Medeea Marinescu, Florin Piersic",
                AvailableTickets = 5
            };

            var nrOfTicketsToSell = 2;
            mockShowRepository.Setup(x => x.GetRecord(show.Id)).Returns(show);

            var truthValue = showLogic.SellTickets(show.ToShowModel(), nrOfTicketsToSell);
            Assert.IsTrue(truthValue);
        }

        [TestMethod]
        public void SellTicketFail()
        {
            var show = new Show()
            {
                Id = 1,
                Name = "Straini in noapte",
                DistributionList = "Medeea Marinescu, Florin Piersic",
                AvailableTickets = 5
            };

            var nrOfTicketsToSell = 6;
            mockShowRepository.Setup(x => x.GetRecord(show.Id)).Returns(show);

            var truthValue = showLogic.SellTickets(show.ToShowModel(), nrOfTicketsToSell);
            Assert.IsTrue(truthValue);
        }

        [TestMethod]
        public void VerifyTicketsSuccess()
        {
            var show = new Show()
            {
                Id = 1,
                Name = "Straini in noapte",
                DistributionList = "Medeea Marinescu, Florin Piersic",
                AvailableTickets = 2
            };

            mockShowRepository.Setup(x => x.GetRecord(show.Id)).Returns(show);

            ShowLogic showLogic = new ShowLogic(mockShowRepository.Object);
            var truthValue = showLogic.VerifyReservation(show.Id);

            Assert.IsTrue(truthValue);
        }

        [TestMethod]
        public void VerifyTicketsFail()
        {
            var show = new Show()
            {
                Id = 1,
                Name = "Straini in noapte",
                DistributionList = "Medeea Marinescu, Florin Piersic",
                AvailableTickets = 0
            };

            mockShowRepository.Setup(x => x.GetRecord(show.Id)).Returns(show);

            var truthValue = showLogic.VerifyReservation(show.Id);

            Assert.IsTrue(truthValue);
        }
    }
}
